package com.praktikum.aplikasisalon;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//Tampilan login//

public class LoginActivity extends Activity {
	EditText username,password;
	ImageView link_ig, link_tw, link_tk;
	TextView about_me;

	private long lastPressedTime;
	private static final int PERIOD = 2000;
	
	@Override
	public void onBackPressed() {
	    super.onBackPressed();
	    this.finish();
	}	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    //Handle the back button
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
	        switch (event.getAction()) {
	        case KeyEvent.ACTION_DOWN:
	            if (event.getDownTime() - lastPressedTime < PERIOD) {
	            	
	                finish();
	            }else {
	                Toast.makeText(getApplicationContext(), "Tekan 2 kali untuk keluar",Toast.LENGTH_SHORT).show();
	                lastPressedTime = event.getEventTime();
	            }
	            return true;
	        }
	    }
	    return false;
	    
	}
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
		
		username = (EditText) findViewById(R.id.text_username);
		password = (EditText) findViewById(R.id.text_pass);


		about_me = (TextView) findViewById(R.id.about_me);
		about_me.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),AboutMeActivity.class);
				startActivity(intent);
			}
		});
		
		
	}

	public void register(View v){
		Intent i = new Intent(getApplicationContext(),ManagUserActivity.class);
		i.putExtra("mode", "Tambah");
		startActivity(i);
		finish();
	}
	
	public void Login (View v){
		if(username.getText().toString().isEmpty()){
			username.setError("Kosong");
		}else if(password.getText().toString().isEmpty()){
			password.setError("Kosong");
		}else{
			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
			ModelUser mdUser = db.getLogin(username.getText().toString(), password.getText().toString());
			
			if(mdUser.getIsLogin()){
				if(mdUser.getTipeUser().equals("SUPER ADMIN")) {
					Intent i = new Intent(getApplicationContext(), MainActivity.class);
					startActivity(i);
					finish();
				}else{
					Intent i = new Intent(getApplicationContext(), PilihLayananActivity.class);
					i.putExtra("id_user", mdUser.getIdUser());
					startActivity(i);
					finish();
				}
			}else{
				Toast.makeText(LoginActivity.this, "Periksa Username atau Password",Toast.LENGTH_LONG).show();
				//TAMBAHDATA
			}
		}
	}
	
	public void Batal (View v){
		finish();
		System.exit(0);
	}
	
	
}
