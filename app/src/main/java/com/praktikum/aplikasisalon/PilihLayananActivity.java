package com.praktikum.aplikasisalon;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PilihLayananActivity extends Activity {
    private long lastPressedTime;
    private static final int PERIOD = 2000;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private String idUser;
    private int biayaCukur = 0;
    private int biayaWarna = 0;
    private int biayaPerawatan = 0;
    private int biayaTotal = 0;
    private int[] selectDate = new int[3];
    private int[] currentDate = new int[3];
    private String[] selectDate_;
    private String[] currentDate_;

    TextView dateView, biayaView;
    Spinner lt_cukur, lt_warna, lt_perawatan;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_DOWN:
                    if (event.getDownTime() - lastPressedTime < PERIOD) {

                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(), "Tekan 2 kali untuk keluar",Toast.LENGTH_SHORT).show();
                        lastPressedTime = event.getEventTime();
                    }
                    return true;
            }
        }
        return false;

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_layanan);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        dateView = (TextView) findViewById(R.id.tv_date_pl);
        biayaView = (TextView) findViewById(R.id.tv_tBiaya_pl);
        lt_cukur = (Spinner) findViewById(R.id.s_tipeCukur_pl);
        lt_warna = (Spinner) findViewById(R.id.s_tipeWarna_pl);
        lt_perawatan = (Spinner) findViewById(R.id.s_tipePerawatan_pl);

        Intent i = this.getIntent();
        idUser =  i.getStringExtra("id_user");
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        ModelUser dataUser = db.getUserById(idUser);

        dateView.setText(dataUser.getLDate());
        selectDate_ = dataUser.getLDate().split("-");
        selectDate[0] = Integer.valueOf(selectDate_[0]);
        selectDate[1] = Integer.valueOf(selectDate_[1]);
        selectDate[2] = Integer.valueOf(selectDate_[2]);

        lt_cukur.setSelection(Integer.valueOf(dataUser.getLTCukur()));
        lt_warna.setSelection(Integer.valueOf(dataUser.getLTWarna()));
        lt_perawatan.setSelection(Integer.valueOf(dataUser.getLTPerawatan()));

        lt_cukur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position == 0){
                    biayaCukur = 0;
                }else if(position == 1){
                    biayaCukur = 25000;
                }else if(position == 2){
                    biayaCukur = 30000;
                }else if(position == 3){
                    biayaCukur = 15000;
                }else if(position == 4){
                    biayaCukur = 20000;
                }else if(position == 5){
                    biayaCukur = 50000;
                }else if(position == 6){
                    biayaCukur = 10000;
                }
                biayaTotal = biayaCukur+biayaWarna+biayaPerawatan;
                biayaView.setText(String.valueOf(biayaTotal));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                lt_cukur.setSelection(0);
                biayaCukur = 0;
                biayaTotal = biayaCukur+biayaWarna+biayaPerawatan;
                biayaView.setText(String.valueOf(biayaTotal));
            }

        });
        lt_warna.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position == 0){
                    biayaWarna = 0;
                }else if(position == 1){
                    biayaWarna = 65000;
                }else if(position == 2){
                    biayaWarna = 65000;
                }else if(position == 3){
                    biayaWarna = 65000;
                }else if(position == 4){
                    biayaWarna = 65000;
                }else if(position == 5){
                    biayaWarna = 65000;
                }else if(position == 6){
                    biayaWarna = 145000;
                }
                biayaTotal = biayaCukur+biayaWarna+biayaPerawatan;
                biayaView.setText(String.valueOf(biayaTotal));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                lt_warna.setSelection(0);
                biayaWarna = 0;
                biayaTotal = biayaCukur+biayaWarna+biayaPerawatan;
                biayaView.setText(String.valueOf(biayaTotal));
            }

        });
        lt_perawatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position == 0){
                    biayaPerawatan = 0;
                }else if(position == 1){
                    biayaPerawatan = 75000;
                }else if(position == 2){
                    biayaPerawatan = 140000;
                }else if(position == 3){
                    biayaPerawatan = 540000;
                }else if(position == 4){
                    biayaPerawatan = 150000;
                }else if(position == 5){
                    biayaPerawatan = 250000;
                }else if(position == 6){
                    biayaPerawatan = 525000;
                }else if(position == 7){
                    biayaPerawatan = 55000;
                }else if(position == 8){
                    biayaPerawatan = 75000;
                }else if(position == 9){
                    biayaPerawatan = 220000;
                }else if(position == 10){
                    biayaPerawatan = 70000;
                }
                biayaTotal = biayaCukur+biayaWarna+biayaPerawatan;
                biayaView.setText(String.valueOf(biayaTotal));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                lt_perawatan.setSelection(0);
                biayaPerawatan = 0;
                biayaTotal = biayaCukur+biayaWarna+biayaPerawatan;
                biayaView.setText(String.valueOf(biayaTotal));
            }

        });
    }

    public void PickDate(View v){

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                String date = dateFormatter.format(newDate.getTime());
                selectDate_ = date.split("-");
                selectDate[0] = Integer.valueOf(selectDate_[0]);
                selectDate[1] = Integer.valueOf(selectDate_[1]);
                selectDate[2] = Integer.valueOf(selectDate_[2]);

                dateView.setText(date);
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();

    }

    public void Simpan(View v){
        Date nowDate = Calendar.getInstance().getTime();
        SimpleDateFormat date_day = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String currentDateS = date_day.format(nowDate);
        currentDate_ = currentDateS.split("-");
        currentDate[0] = Integer.valueOf(currentDate_[0]);
        currentDate[1] = Integer.valueOf(currentDate_[1]);
        currentDate[2] = Integer.valueOf(currentDate_[2]);

        if(lt_cukur.getSelectedItemPosition()==0 && lt_warna.getSelectedItemPosition()==0 && lt_perawatan.getSelectedItemPosition()==0){
            Toast.makeText(PilihLayananActivity.this, "Anda belum memilih layanan apapun! (Minimal 1)",Toast.LENGTH_LONG).show();
        }else{
            Intent in = this.getIntent();
            idUser =  in.getStringExtra("id_user");
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            ModelUser dataUser = db.getUserById(idUser);

            dataUser.setLDate(dateView.getText().toString());
            dataUser.setLTCukur(String.valueOf(lt_cukur.getSelectedItemPosition()));
            dataUser.setLTWarna(String.valueOf(lt_warna.getSelectedItemPosition()));
            dataUser.setLTPerawatan(String.valueOf(lt_perawatan.getSelectedItemPosition()));

            int status = db.prosesUpdate(dataUser);
            if(status>0){
                Toast.makeText(PilihLayananActivity.this, "Data Berhasil Disimpan ",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(PilihLayananActivity.this, "Data Gagal Disimpan ",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void Kembali(View v){
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }

}
