package com.praktikum.aplikasisalon;

public class ModelUser {

	private String idUser;
	private String nama;
	private String username;
	private String password;
	private String email;
	private String tipeUser;
	private String no;
	private Boolean isLogin;

	private String LDate;
	private String LTCukur;
	private String LTWarna;
	private String LTPerawatan;

	//// FUNGSI ////

	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipeUser() {
		return tipeUser;
	}
	public void setTipeUser(String tipeUser) {
		this.tipeUser = tipeUser;
	}

	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}
	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	//// DATA LAYANAN PADA SALON ////

	public String getLDate() {
		return LDate;
	}
	public void setLDate(String LDate) {
		this.LDate = LDate;
	}

	public String getLTCukur() {
		return LTCukur;
	}
	public void setLTCukur(String LTCukur) {
		this.LTCukur = LTCukur;
	}

	public String getLTWarna() {
		return LTWarna;
	}
	public void setLTWarna(String LTWarna) {
		this.LTWarna = LTWarna;
	}

	public String getLTPerawatan() {
		return LTPerawatan;
	}
	public void setLTPerawatan(String LTPerawatan) {
		this.LTPerawatan = LTPerawatan;
	}

}
