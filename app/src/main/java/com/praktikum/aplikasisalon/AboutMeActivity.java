package com.praktikum.aplikasisalon;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;

public class AboutMeActivity extends Activity {
        public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_about_me);

                if (android.os.Build.VERSION.SDK_INT > 9) {
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                }
        }

        public void kembali (View v){
                finish();
        }
}
